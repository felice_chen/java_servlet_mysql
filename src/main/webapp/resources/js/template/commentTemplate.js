function generateCommentTemplate(comment, fileIconDisplay, commonBtnDisplay) {
	return '<article class="post comment comment' + comment["commentId"]
			+ '" userId="' + comment["userId"] + '" commentId ="'
			+ comment["commentId"] + '">'
			+ '<span class="published comment-post-time">'
			+ comment["post_date"] + '</span><br>'
			+ '<span class="comment-username">' + comment["userName"]
			+ '</span>：' + '<span class="commentContent">' + comment["content"]
			+ '</span>' + '<button class="fa fa-paperclip comment-file-btn '
			+ fileIconDisplay + '" onclick=\"viewCommentFile(this)\"></button>'
			+ '<button class="fa fa-pencil circleIconBtn comment-update-btn '
			+ commonBtnDisplay + '" onclick=\"updateComment(this)\"></button>'
			+ '<button class="fa fa-trash circleIconBtn comment-del-btn '
			+ commonBtnDisplay + '" onclick=\"delComment(this)\"></button>'
			+ '</article>';
}

function generateFileInputColumnTemplate(name) {
	return '<input type="file" name="' + name
			+ '" size="50" maxlength="20" style="display: inline;" />';
}

function generateFileTableColumnTemplate(files, delFileIconDisplay) {
	return '<tr class="commentFileContent" commentFileId ="' + files["commentFileId"] + '" url="'+ files["url"]+'">'
	+ '<td><button class="fa fa-trash '+ delFileIconDisplay +'" style="border: none;" onclick="openConfirmCommentFileModal(this)"></button></td>'
	+ '<td>'
	+'<a href="/workshop/downloadFileByCommentFileId?commentFileId=' + files["commentFileId"] + '">'
	+ files["fileName"]
	+'</a></td>' + '</tr>';
}