var userId = getSessionValueByKey("userId");
var username = getSessionValueByKey("account");
var owner = $.url("?owner");
var articleId = $.url("?articleId");

$(document).ready(function() {
	initArticlePage();
	getAllComment();
});

function initArticlePage() {
	if (userId) {
		$("#commentBtn").css("display", "");
	} else {
		$("#commentBtn").css("display", "none");
	}
}

function getAllComment() {
	$.ajax({
		url : "commentServlet/findByArticleId",
		dataType : "json",
		data : {
			articleId : articleId
		}
	}).done(
			function(comments) {
				for (var i = 0; i < comments.length; i++) {
					var comment = comments[i];
					var fileIconClass = getFileIconClass(comment.commentFiles);
					var tempalte;
					if (comment["userId"] == userId) {
						tempalte = generateCommentTemplate(comment,
								fileIconClass, "");
					} else {
						tempalte = generateCommentTemplate(comment,
								fileIconClass, "comment-non-display");
					}
					$("#main").append(tempalte);
				}

			}).fail(function(data, status, headers, config) {
		console.log("error!");
	});
}

function submitComment() {
	var content = $("#commentContent").val();
	var articleId = 1;

	userId = getSessionValueByKey("userId");
	var formData = new FormData($('#aa')[0]);
	formData.append('content', content);
	formData.append('userId', userId);
	formData.append('articleId', articleId);
	$.ajax({
		type : "POST",
		url : "commentServlet/createComment",
		data : formData,
		dataType : "json",
		processData : false,
		contentType : false,
		success : function(response) {
			if (response["errorMessage"] != null
					&& response["errorMessage"].length > 0) {
				messageBox("errorMessage", response["errorMessage"], 3000);
			} else {
				$("#commentModal").modal("hide");
				messageBox("successMessage", "留言成功!", 3000);
				var fileIconClass = getFileIconClass(response.commentFiles);
				var template = generateCommentTemplate(response, fileIconClass,
						"");
				$("#main").append(template);
			}
		},
		error : function(a) {
			console.log("error");
			messageBox("errorMessage", "留言失敗!", 3000);
		}
	});
}

function delComment(_this) {
	$("#delCommentModal").modal("show");
	var comment = $(_this).parent("article");
	var commentId = comment.attr("commentId");

	$("#delCommentYesBtn").off("click");
	$("#delCommentYesBtn").click(function() {
		$.ajax({
			url : "commentServlet/delByCommentId",
			data : {
				commentId : commentId
			}
		}).done(function() {
			$("#delCommentModal").modal("hide");
			comment[0].remove();
			messageBox("successMessage", "刪除成功!", 3000);
		}).fail(function(data, status, headers, config) {
			messageBox("errorMessage", "刪除失敗!", 3000);
		});
	});
}

function updateComment(_this) {
	$("#updateCommentModal").modal("show");
	var comment = $(_this).parent("article");
	var contentElement = comment.find(".commentContent");
	var oriContent = contentElement.text();
	$("#updateCommentContent").val(oriContent);
	$("#updateCommentOkBtn").off("click");
	$("#updateCommentOkBtn").click(function() {
		var commentId = comment.attr("commentId");
		var newContent = $("#updateCommentContent").val();
		$.ajax({
			url : "commentServlet/updateByCommentIdAndContent",
			data : {
				commentId : commentId,
				content : newContent
			}
		}).done(function() {
			contentElement.text(newContent);
			$("#updateCommentModal").modal("hide");
			messageBox("successMessage", "修改成功!", 3000);
		}).fail(function(data, status, headers, config) {
			messageBox("errorMessage", "修改失敗!", 3000);
		});
	})
}

function viewCommentFile(_this) {
	
	$(".commentFileContent").remove();
	var comment = $(_this).parent("article");
	var commentId = comment.attr("commentId");

	userId = getSessionValueByKey("userId");
	var commentOwnerId = comment.attr("userId");
	var delFileIconDisplay = "comment-non-display"
	if(commentOwnerId == userId){
		delFileIconDisplay = "";
	}
	
	$.ajax({
		url : "commentFileServlet/findByCommentId",
		dataType:"json",
		data : {
			commentId : commentId
		}
	}).done(function(files) {
		$("#fileCommentModal").modal("show");
		$("#commentFileTable").attr("commentId", commentId);
		for(var i = 0 ;i<files.length ; i++){
			$("#commentFileTable").append(generateFileTableColumnTemplate(files[i], delFileIconDisplay));
		}
		
	}).fail(function(data, status, headers, config) {
		console.log("get comment file list error!");
	});
}

function openConfirmCommentFileModal(_this) {
	$("#delCommentFileModal").modal("show");

	var commentFileTr = $(_this).closest("tr");
	var commentFileId = commentFileTr.attr("commentFileId");

	$("#delCommentFileYesBtn").off("click");
	$("#delCommentFileYesBtn").click(function() {
		$.ajax({
			url : "commentFileServlet/delByCommentFileId",
			data : {
				commentFileId : commentFileId
			}
		}).done(function() {
			$("#delCommentFileModal").modal("hide");
			commentFileTr.remove();
			messageBox("successMessage", "刪除成功!", 3000);
			if($(".commentFileContent").length == 0){
				$("#fileCommentModal").modal("hide");
				var commentId = $("#commentFileTable").attr("commentId");
				$(".comment" + commentId).find(".comment-file-btn").addClass("comment-non-display");
			}
			
		}).fail(function(data, status, headers, config) {
			messageBox("errorMessage", "刪除失敗!", 3000);
		});
	});
}

function appendFileColumn() {
	var index = $("#fileContainer").find("input").length + 1;
	var name = "File" + index;
	$("#fileContainer").append(
			"<br>File " + index + " " + generateFileInputColumnTemplate(name));
}

function getFileIconClass(commentFiles) {
	var fileIconClass = "comment-non-display";
	if (commentFiles.length > 0) {
		fileIconClass = "";
	}

	return fileIconClass;
}
