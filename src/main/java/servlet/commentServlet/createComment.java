package servlet.commentServlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import config.initServlet;
import model.CommentModel;
import service.CommentService;

@WebServlet("/commentServlet/createComment")
public class createComment extends initServlet {
	private static final long serialVersionUID = 1L;
	private Gson gson = new Gson();

	@Autowired
	CommentService commentService;

	public createComment() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		String resultJson;
		try {
			CommentModel partitionCM = commentService.uploadCommentFiles(request, response);

			if (partitionCM.getErrorMessage() != null && partitionCM.getErrorMessage().length() > 0) {
				resultJson = this.gson.toJson(partitionCM);
			} else {
				CommentModel completeCM = commentService.createComment(partitionCM);
				resultJson = this.gson.toJson(completeCM);
			}
			response.getWriter().print(resultJson);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
