package servlet.commentServlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import config.initServlet;
import model.CommentModel;
import service.CommentFileService;
import service.CommentService;

@WebServlet("/commentServlet/findByArticleId")
public class findByArticleId extends initServlet {
	private static final long serialVersionUID = 1L;
	private Gson gson = new Gson();

	@Autowired
	CommentService commentService;

	@Autowired
	CommentFileService commentFileService;

	public findByArticleId() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String articleId = request.getParameter("articleId");

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		try {
			List<CommentModel> cms = commentService.findAllByArticleId(articleId);
			List<CommentModel> completeCMs = commentFileService.findByCommentIds(cms);
			String resultJson = this.gson.toJson(completeCMs);
			response.getWriter().print(resultJson);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
