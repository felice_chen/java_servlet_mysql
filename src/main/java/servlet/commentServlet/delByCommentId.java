package servlet.commentServlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import config.initServlet;
import service.CommentService;

@WebServlet("/commentServlet/delByCommentId")
public class delByCommentId extends initServlet {
	private static final long serialVersionUID = 1L;
       
	@Autowired
	CommentService commentService;
	
    public delByCommentId() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String commentId = request.getParameter("commentId");
		try {
			commentService.delByCommentId(commentId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
