package servlet.userServlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import config.initServlet;
import service.UserService;

@WebServlet("/userServlet/findNameByAccount")
public class findNameByAccount extends initServlet {
	private static final long serialVersionUID = 1L;
       
	@Autowired
	UserService userService;
	
    public findNameByAccount() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String account = request.getParameter("account");
		try {
			String name = userService.findNameByAccount(account);
			response.getWriter().append(name);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
