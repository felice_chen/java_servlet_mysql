package servlet.userServlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import config.initServlet;
import model.UserModel;
import service.UserService;

@WebServlet("/userServlet/createUser")
public class createUser extends initServlet {
	private static final long serialVersionUID = 1L;
       
	@Autowired
	UserService userService;
	
    public createUser() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		UserModel userModel = new UserModel(name, email, account, password);
		try {
			String userId = userService.createUser(userModel);
			response.getWriter().append(userId);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
