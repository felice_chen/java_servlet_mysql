package servlet.commentFileServlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import config.initServlet;
import model.CommentFileModel;
import service.CommentFileService;

@WebServlet("/commentFileServlet/downloadFileByCommentFileId")
public class downloadFileByCommentFileId extends initServlet {
	private static final long serialVersionUID = 1L;
       
	@Autowired
	CommentFileService commentFileService;
	
    public downloadFileByCommentFileId() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		String commentFileId = request.getParameter("commentFileId");
		String commentFileId = request.getParameter("commentFileId");
		try {
//			String contextPath = request.getContextPath();
			CommentFileModel cfm = commentFileService.findByCommentFileId(commentFileId);
			commentFileService.downloadCommentFiles(request, response, cfm);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
