package servlet.commentFileServlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import config.initServlet;
import service.CommentFileService;

@WebServlet("/commentFileServlet/delByCommentFileId")
public class delByCommentFileId extends initServlet {
	private static final long serialVersionUID = 1L;
       
	@Autowired
	CommentFileService commentFileService;
	
    public delByCommentFileId() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String commentFileId = request.getParameter("commentFileId");
		try {
			commentFileService.delByCommentFileId(commentFileId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
