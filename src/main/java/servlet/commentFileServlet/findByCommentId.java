package servlet.commentFileServlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import config.initServlet;
import model.CommentFileModel;
import service.CommentFileService;

@WebServlet("/commentFileServlet/findByCommentId")
public class findByCommentId extends initServlet {
	private static final long serialVersionUID = 1L;
	private Gson gson = new Gson();

	@Autowired
	CommentFileService commentFileService;

	public findByCommentId() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String commentId = request.getParameter("commentId");

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		try {
			List<CommentFileModel> cfms = commentFileService.findByCommentId(commentId);
			String resultJson = this.gson.toJson(cfms);
			response.getWriter().print(resultJson);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
