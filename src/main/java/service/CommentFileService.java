package service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import dao.CommentFileDao;
import model.CommentFileModel;
import model.CommentModel;
import model.DBModel;

public class CommentFileService {

	@Autowired
	CommentFileDao commentFileDao;

	public List<CommentFileModel> findByCommentId(String commentId) throws SQLException {
		DBModel dbModel = commentFileDao.findByCommentId(commentId);
		ResultSet commentFiles = dbModel.getResultSet();

		List<CommentFileModel> cfms = new ArrayList<CommentFileModel>();
		while (commentFiles.next()) {
			CommentFileModel cfm = new CommentFileModel();
			cfm.setCommentFileId(commentFiles.getString("commentFile_id"));
			cfm.setFileName(commentFiles.getString("fileName"));
			cfm.setUrl(commentFiles.getString("url"));
			cfms.add(cfm);
		}

		dbModel.close();
		return cfms;
	}

	public void delByCommentFileId(String commentFileId) throws SQLException {
		commentFileDao.delByCommentFileId(commentFileId);
	}

	public List<CommentModel> findByCommentIds(List<CommentModel> cms) throws SQLException {

		List<String> commentIds = new ArrayList<String>();
		for (CommentModel cm : cms) {
			String commentId = cm.getCommentId();
			commentIds.add(commentId);
		}

		// 批次查詢 之後再分類成 map
		DBModel dbModel = commentFileDao.findByCommentIds(commentIds);
		ResultSet commentFiles = dbModel.getResultSet();
		MultiValueMap<String, CommentFileModel> commentFilesMap = new LinkedMultiValueMap<String, CommentFileModel>();
		while (commentFiles.next()) {
			CommentFileModel cfm = new CommentFileModel();
			cfm.setCommentFileId(commentFiles.getString("commentFile_id"));
			cfm.setFileName(commentFiles.getString("fileName"));
			cfm.setUrl(commentFiles.getString("url"));
			commentFilesMap.add(commentFiles.getString("comment_id"), cfm);
		}

		for (CommentModel cm : cms) {
			String commentId = cm.getCommentId();
			List<CommentFileModel> cfms = commentFilesMap.get(commentId);
			if (cfms != null) {
				cm.setCommentFiles(cfms);
			}
		}
		dbModel.close();
		return cms;
	}

	public CommentFileModel findByCommentFileId(String commentFileId) throws Exception {
		DBModel dbModel = commentFileDao.findByCommentFileId(commentFileId);
		ResultSet commentFiles = dbModel.getResultSet();

		CommentFileModel cfm = new CommentFileModel();
		if (commentFiles.next()) {
			cfm.setCommentFileId(commentFiles.getString("commentFile_id"));
			cfm.setFileName(commentFiles.getString("fileName"));
			cfm.setUrl(commentFiles.getString("url"));
		}
		dbModel.close();
		return cfm;
	}

	public void downloadCommentFiles(HttpServletRequest request, HttpServletResponse response, CommentFileModel commentFileModel)
			throws Exception {

		String fileName = commentFileModel.getFileName();
		if (fileName == null || fileName.equals("")) {
			throw new ServletException("File Name can't be null or empty");
		}

		String path = commentFileModel.getUrl();
		File file = new File(path);
		if (!file.exists()) {
			throw new ServletException("File doesn't exists on server.");
		}
		System.out.println("File location on server::" + path);
		ServletContext ctx = request.getSession().getServletContext();
		InputStream fis = new FileInputStream(file);
		String mimeType = ctx.getMimeType(file.getAbsolutePath());
		response.setContentType(mimeType != null ? mimeType : "application/octet-stream");
		response.setContentLength((int) file.length());
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

		ServletOutputStream os = response.getOutputStream();
		byte[] bufferData = new byte[1024];
		int read = 0;
		while ((read = fis.read(bufferData)) != -1) {
			os.write(bufferData, 0, read);
		}
		os.flush();
		os.close();
		fis.close();
		System.out.println("File downloaded at client successfully");
	}
}
