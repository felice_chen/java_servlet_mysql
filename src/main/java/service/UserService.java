package service;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;

import dao.UserDao;
import model.DBModel;
import model.UserModel;

public class UserService {

	@Autowired
	UserDao userDao;
	
	public String findIdByAccountAndPassword(String account,String password) throws Exception{
		String userId = userDao.findIdByAccountAndPassword(account, password);
		
		if(userId == null) {
			throw new Exception("帳號或密碼錯誤或無此用戶");
		}
		return userId;
	}
	
	public String createUser(UserModel userModel) throws Exception{
		
		DBModel dbModel = userDao.findByAccount(userModel.getAccount());
		boolean isExistAccount = dbModel.getResultSet().first();
		dbModel.close();
		
		if(isExistAccount) {
			throw new Exception("帳號已被使用");
		}
		return userDao.createUser(userModel);
	}
	
	public String findNameById(String userId) throws SQLException{
		return userDao.findNameById(userId);
	}
	
	public String findNameByAccount(String account) throws SQLException{
		return userDao.findNameByAccount(account);
	}
	
	public String findIdByAccount(String account) throws SQLException{
		return userDao.findIdByAccount(account);
	}
}
