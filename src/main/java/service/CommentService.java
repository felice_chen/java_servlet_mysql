package service;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.springframework.beans.factory.annotation.Autowired;

import dao.CommentDao;
import model.CommentFileModel;
import model.CommentModel;
import model.DBModel;
import tool.UploadTool;

public class CommentService {

	@Autowired
	UserService userService;

	@Autowired
	CommentDao commentDao;

	public List<CommentModel> findAllByArticleId(String articleId) throws SQLException {

		DBModel dbModel = commentDao.findAllByArticleId(articleId);
		ResultSet comments = dbModel.getResultSet();

		List<CommentModel> cms = new ArrayList<CommentModel>();
		while (comments.next()) {
			CommentModel cm = new CommentModel();
			String userId = comments.getString("user_id");
			cm.setCommentId(comments.getString("comment_id"));
			cm.setContent(comments.getString("content"));
			cm.setPost_date(comments.getString("post_date"));
			cm.setUserId(userId);
			cm.setUserName(userService.findNameById(userId));
			cms.add(cm);
		}

		dbModel.close();
		return cms;
	}

	public CommentModel createComment(CommentModel cm) throws SQLException {

		DBModel dbModel = commentDao.createComment(cm.getContent(), cm.getUserId(), cm.getArticleId());
		ResultSet comment = dbModel.getResultSet();
		if (comment.next()) {
			cm.setCommentId(comment.getString("comment_id"));
			cm.setPost_date(comment.getString("post_date"));
			String userId = cm.getUserId();
			cm.setUserName(userService.findNameById(userId));
		}
		dbModel.close();
		
		return commentDao.saveCommentFile(cm);
	}

	public void delByCommentId(String commentId) throws SQLException {
		commentDao.delByCommentId(commentId);
	}

	public void updateByCommentIdAndContent(String commentId, String content) throws SQLException {
		commentDao.updateByCommentIdAndContent(commentId, content);
	}

	public CommentModel uploadCommentFiles(HttpServletRequest request, HttpServletResponse response) throws Exception {
 		CommentModel cm = new CommentModel();
 		List<CommentFileModel> cfms = new ArrayList<CommentFileModel>();
		
		UploadTool upload = new UploadTool(request);
		String msg = upload.checkUpload();
		if (msg.length() > 0) {
			cm.setErrorMessage(msg);
			System.out.println(msg);
			return cm;
		} else {
			
			// 設定上傳路徑
//			String uploadDirPath = ServletActionContext.getServletContext().getRealPath("/upload");
			String uploadDirPath = request.getSession().getServletContext().getRealPath(".") + "/uploadFile/";
			File file = new File(uploadDirPath);
			if (!file.exists()) {
				file.mkdir();
			}
			
			upload.setUploadDir(uploadDirPath);
			
			// 開始上傳
			Map uploadFiles = upload.getAllUploadFileList();
			Iterator iter = uploadFiles.keySet().iterator();
			while (iter.hasNext()) {
				CommentFileModel cfm = new CommentFileModel();
				String inputName = iter.next().toString();
				FileItem item = (FileItem) uploadFiles.get(inputName);
				String fileName = item.getName();
				msg = upload.doUpload(item);
				cm.setErrorMessage(msg);
				
				cfm.setFileName(fileName);
				cfm.setUrl(uploadDirPath + fileName);
				cfms.add(cfm);
			}
			
			Map otherColumns = upload.getOtherColumns();
			cm.setUserId(otherColumns.get("userId").toString());
			cm.setArticleId(otherColumns.get("articleId").toString());
			cm.setContent(otherColumns.get("content").toString());
			cm.setCommentFiles(cfms);
		}
		
		return cm;
	}

}
