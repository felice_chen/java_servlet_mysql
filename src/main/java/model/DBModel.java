package model;

import java.sql.Connection;
import java.sql.ResultSet;

import config.DBConfig;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DBModel {

	DBConfig dbConfig;
	
	Connection connection;

	ResultSet resultSet;

	public DBModel(DBConfig dbConfig, Connection con, ResultSet rs) {
		this.dbConfig = dbConfig;
		this.connection = con;
		this.resultSet = rs;
	}
	
	public void close() {
		dbConfig.close(this.connection);
	}
}
