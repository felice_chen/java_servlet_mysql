package model;

import lombok.Getter;  
import lombok.Setter;  

@Getter
@Setter
public class UserModel {

	String name;

	String email;
	
	String account;
	
	String password;

	public UserModel(String name, String email, String account, String password) {
		this.name = name;
		this.email = email;
		this.account = account;
		this.password = password;
	}
}
