package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import config.DBConfig;
import model.DBModel;
import model.UserModel;

public class UserDao {

	public String findIdByAccountAndPassword(String account, String password) throws SQLException {
		DBConfig dbConfig = new DBConfig();
		Connection con = dbConfig.conDB();
		Statement st = con.createStatement();
		st.executeQuery("SELECT * FROM user WHERE account = '" + account + "' and password = '" + password + "'");

		ResultSet resultSet = st.getResultSet();
		String userId = null;
		if (resultSet.next()) {
			userId = st.getResultSet().getString("user_id").toString();
		}

		dbConfig.close(con);
		return userId;
	}

	public DBModel findByAccount(String account) throws SQLException {
		DBConfig dbConfig = new DBConfig();
		Connection con = dbConfig.conDB();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("SELECT * FROM user WHERE account = '" + account + "'");
		return new DBModel(dbConfig, con, rs);
	}

	public String createUser(UserModel userModel) throws SQLException {
		DBConfig dbConfig = new DBConfig();
		Connection con = dbConfig.conDB();
		Statement st = con.createStatement();
		st.executeUpdate("INSERT INTO user (name, email, account , password) VALUE('" + userModel.getName() + "', '"
				+ userModel.getEmail() + "', '" + userModel.getAccount() + "' ,'" + userModel.getPassword() + "');",
				Statement.RETURN_GENERATED_KEYS);

		String userId = null;
		ResultSet rs = st.getGeneratedKeys();
		if (rs.next()) {
			userId = rs.getString(1);
		} else {
			// throw an exception from here
		}

		dbConfig.close(con);
		return userId;
	}

	public String findNameById(String userId) throws SQLException {
		DBConfig dbConfig = new DBConfig();
		Connection con = dbConfig.conDB();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("SELECT * FROM user WHERE user_id = '" + userId + "'");

		String name = null;
		if (rs.next()) {
			name = rs.getString("name");
		}

		dbConfig.close(con);
		return name;
	}

	public String findNameByAccount(String account) throws SQLException {
		DBConfig dbConfig = new DBConfig();
		Connection con = dbConfig.conDB();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("SELECT * FROM user WHERE account = '" + account + "'");

		String name = null;
		if (rs.next()) {
			name = rs.getString("name");
		}

		dbConfig.close(con);
		return name;
	}

	public String findIdByAccount(String account) throws SQLException {
		DBConfig dbConfig = new DBConfig();
		Connection con = dbConfig.conDB();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("SELECT * FROM user WHERE account = '" + account + "'");

		String name = null;
		if (rs.next()) {
			name = rs.getString("user_id");
		}

		dbConfig.close(con);
		return name;
	}
}
