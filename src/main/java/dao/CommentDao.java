package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import config.DBConfig;
import model.CommentFileModel;
import model.CommentModel;
import model.DBModel;

public class CommentDao {

	public DBModel createComment(String content, String userId, String articleId) throws SQLException {
		Date date = new Date();
		String nowDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);

		DBConfig dbConfig = new DBConfig();
		Connection con = dbConfig.conDB();
		Statement st = con.createStatement();
		st.executeUpdate("INSERT INTO comment (content, post_date, user_id , article_id, authotity, status) VALUE('"
				+ content + "', '" + nowDateTime + "','" + userId + "', '" + articleId + "', 'public', 'exist');");

		// TODO 要改 儲存完畢就可回傳儲存的資料 ， 而不是在去撈一次
		ResultSet rs = st.executeQuery("SELECT * FROM comment Order By post_date DESC limit 1");
		return new DBModel(dbConfig, con, rs);
	}

	public CommentModel saveCommentFile(CommentModel cm) throws SQLException {
		DBConfig dbConfig = new DBConfig();
		Connection con = dbConfig.conDB();
		Statement st = con.createStatement();

		List<CommentFileModel> commentFiles = cm.getCommentFiles();
		for (CommentFileModel commentFile : commentFiles) {
			st.executeUpdate(
					"INSERT INTO commentFile (fileName, url, comment_id) VALUE('" + commentFile.getFileName() + "', '"
							+ commentFile.getUrl() + "','" + cm.getCommentId() + "');",
					Statement.RETURN_GENERATED_KEYS);

			String commentFileId = null;
			ResultSet rs = st.getGeneratedKeys();
			if (rs.next()) {
				commentFileId = rs.getString(1);
			} else {
				// throw an exception from here
			}
			commentFile.setCommentFileId(commentFileId);
		}

		dbConfig.close(con);
		return cm;
	}

	public DBModel findAllByArticleId(String articleId) throws SQLException {

		DBConfig dbConfig = new DBConfig();
		Connection con = dbConfig.conDB();
		Statement st = con.createStatement();
		ResultSet rs = st
				.executeQuery("SELECT * FROM comment WHERE article_id = '" + articleId + "' Order By post_date");
		return new DBModel(dbConfig, con, rs);
	}

	public void delByCommentId(String commentId) throws SQLException {
		DBConfig dbConnection = new DBConfig();
		Connection con = dbConnection.conDB();
		Statement st = con.createStatement();
		st.execute("DELETE FROM commentFile WHERE comment_id = '" + commentId + "'");
		st.execute("DELETE FROM comment WHERE comment_id = '" + commentId + "'");
		dbConnection.close(con);
	}

	public void updateByCommentIdAndContent(String commentId, String content) throws SQLException {
		DBConfig dbConnection = new DBConfig();
		Connection con = dbConnection.conDB();
		Statement st = con.createStatement();
		st.execute("UPDATE comment SET content = '" + content + "' WHERE comment_id = '" + commentId + "'");
		dbConnection.close(con);
	}
}
