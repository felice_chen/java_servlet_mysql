package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import config.DBConfig;
import model.DBModel;

public class CommentFileDao {
	
	public DBModel findByCommentId(String commentId) throws SQLException {
		DBConfig dbConfig = new DBConfig();
		Connection con = dbConfig.conDB();
		Statement st = con.createStatement();
		ResultSet rs = st
				.executeQuery("SELECT * FROM commentFile WHERE comment_id = '" + commentId + "'");
		return new DBModel(dbConfig, con, rs);
	}

	public DBModel findByCommentIds(List<String> commentIds) throws SQLException {
		DBConfig dbConfig = new DBConfig();
		Connection con = dbConfig.conDB();
		Statement st = con.createStatement();
		
		String commentIdsString = StringUtils.join(commentIds, ",");
		ResultSet rs = st.executeQuery("SELECT * FROM commentFile WHERE comment_id IN (" + commentIdsString + ")");
		
		return new DBModel(dbConfig, con, rs);
	}
	
	public void delByCommentFileId(String commentFileId) throws SQLException {
		DBConfig dbConnection = new DBConfig();
		Connection con = dbConnection.conDB();
		Statement st = con.createStatement();
		st.execute("DELETE FROM commentFile WHERE commentFile_id = '" + commentFileId + "'");
		dbConnection.close(con);
	}
	
	public DBModel findByCommentFileId(String commentFileId) throws SQLException {
		DBConfig dbConfig = new DBConfig();
		Connection con = dbConfig.conDB();
		Statement st = con.createStatement();
		ResultSet rs = st
				.executeQuery("SELECT * FROM commentFile WHERE commentFile_id = '" + commentFileId + "'");
		return new DBModel(dbConfig, con, rs);
	}
}
